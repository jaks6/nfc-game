package ee.ut.cs.mc.ia;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

public class HandlerMain extends Handler {
	
	public static final int DISPLAY_TOAST = 1;
	
	Activity activity;

	public HandlerMain(Activity activity) {
		this.activity = activity;
	}

	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		
		switch (msg.what){
		case DISPLAY_TOAST:
			
			Context context = App.getContext();
			String toastText = (String) msg.obj;
			int toastDuration = Toast.LENGTH_LONG;
			
			Toast toast = Toast.makeText(context, toastText, toastDuration);
			toast.show();
			
			break;
		}
	}
}
	
