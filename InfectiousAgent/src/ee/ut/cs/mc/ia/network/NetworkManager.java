package ee.ut.cs.mc.ia.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import ee.ut.cs.mc.ia.App;
import ee.ut.cs.mc.ia.PreferencesHelper;
import ee.ut.cs.mc.ia.Virus;
import ee.ut.cs.mc.ia.activity.LoginActivity;
import ee.ut.cs.mc.ia.activity.MainActivity;
import ee.ut.cs.mc.ia.gui.dialogs.LoadingDialog;
import ee.ut.cs.mc.ia.gui.fragments.MyVirusFragment;

public class NetworkManager {
	Handler mHandler;
	SharedPreferences prefs;

	public NetworkManager(Handler handler){
		this.mHandler = handler;
	}

	public NetworkManager() {
	}

	boolean responseRead = false;
	private String TAG = "NetworkManager";



	public String readOutputFromServer(URLConnection connection)
			throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String returnString="";
		StringBuffer response = new StringBuffer();
		while ((returnString = in.readLine()) != null)
		{
			Log.d("Returned string", returnString);
			response.append(returnString);
		}
		in.close();
		
		return response.toString();
	}

	public String getDefaultUrl(){
		return "http://infectious-agent.appspot.com/db";
	}

	public void storeFromJSON(JSONObject json, String requestType, Handler handler){
		String baseURL = "http://infectious-agent.appspot.com/db?";
		Iterator<?> keys = json.keys();
		Log.i(TAG,json.toString());
		try {
			while( keys.hasNext() ){
				String key = (String)keys.next();

				if( json.get(key) instanceof String ){
					baseURL += key + "=" + json.get(key) + "&";
				}
			}
			if(requestType=="POST"){
				URL url = new URL(baseURL.replace(" ", "%20"));

				new PostVirusRequest(handler, json).execute(url);

			}
			else{

			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void executeGetVirusTask(Context context) {
		new GetVirusTask(context).execute();
	}
	
	public class GetVirusTask extends AsyncTask<Void, Void, JSONObject> {
		Context context;
		LoadingDialog loading;
		
		final String requestParams;
		String useremail;
		String serverURL;
		
		public GetVirusTask(Context context){
			this.context = context;
			useremail = App.getStringFromPrefs(PreferencesHelper.LAST_USER_EMAIL);
			requestParams = "?type=getvirus&email=" + useremail;
		}
		
		protected JSONObject doInBackground(Void... params) {
			JSONObject result = null;
			try {
				URL url = null;
				serverURL = App.getNetworkManager().getDefaultUrl();
				url = new URL(serverURL + requestParams);
				result =  createJSONfromRequest(url);
				
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return result;
		}
		
		protected void onPreExecute(){
			loading = new LoadingDialog(
					context, "Loading", "Fetching virus info");
		}
		protected void onPostExecute(JSONObject obj){
			try {
				App.setUserVirus(obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			//Dismiss loading dialog
			loading.dismiss();
			
			//Open mainActivity
			if(!LoginActivity.LOGGED_IN) {
				Intent intent = new Intent(context, MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				context.startActivity(intent);
				
				LoginActivity.LOGGED_IN = true;
			}
		}
	}
	
	
	public JSONObject createJSONfromRequest(URL url) throws JSONException, IOException {
			HttpURLConnection con = (HttpURLConnection) url.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			Log.v(TAG, ("\nSending 'GET' request to URL : " + url));
			Log.v(TAG, ("Response Code : " + responseCode));

			if (responseCode==404) {
				return null;
			}
		JSONObject obj = new JSONObject(readOutputFromServer(con));
		return obj;
	}
	


	private class PostVirusRequest extends AsyncTask<URL, Void, String> {
		Handler handler;
		JSONObject json;
		public PostVirusRequest(Handler handler, JSONObject json){

			this.handler = handler;
			this.json = json;
		}
		@Override
		protected String doInBackground(URL... params) {
			Log.i(TAG, params[0].toString());
			HttpURLConnection httpUrlConnection = null;
			
			try {
				httpUrlConnection = (HttpURLConnection) params[0].openConnection();
				httpUrlConnection.setRequestMethod("POST");
				httpUrlConnection.connect();
				int responseCode = httpUrlConnection.getResponseCode();
				Log.v(TAG, ("Response Code : " + responseCode));
				return readOutputFromServer(httpUrlConnection);
			} catch (ProtocolException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		@Override
		protected void onPostExecute(String virusID) {
			if (handler != null && json != null){
				try {
					json.put("id",virusID);
					App.setUserVirus(new Virus(json));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				Message msg =
                        handler.obtainMessage(MyVirusFragment.MSG_UPDATE_UI);
				msg.sendToTarget();

			}
			//Refresh textviews in loginactvity
		}
	}
	
	

	private void getLocationFromJSON(JSONObject obj) {
		// TODO Auto-generated method stub
		
	}
	
	
	public class GetLocationTask extends AsyncTask<URL, Void, JSONObject> {
		protected JSONObject doInBackground(URL... urls) {
			try {
				return createJSONfromRequest(urls[0]);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		protected void onPostExecute(JSONObject obj){
			getLocationFromJSON(obj);
		}
		
	}
}
