package ee.ut.cs.mc.ia.gui.fragments;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ee.ut.cs.mc.ia.App;
import ee.ut.cs.mc.ia.PreferencesHelper;
import ee.ut.cs.mc.ia.R;
import ee.ut.cs.mc.ia.network.NetworkManager.GetVirusTask;

public class MapFragment extends Fragment implements LocationListener {
	MapView mapView;
	GoogleMap map;
	public String TAG = "MapFragment";
	public static double latitude;  
	public static double longitude;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {


		View v = inflater.inflate(R.layout.mapfragment, container,false);
		Location l = App.getLocation();
		latitude = l.getLatitude();
		longitude = l.getLongitude();

		// Gets the MapView from the XML layout and creates it
		mapView = (MapView) v.findViewById(R.id.mapView);
		mapView.onCreate(savedInstanceState);

		// Gets to GoogleMap from the MapView and does initialization stuff
		map = mapView.getMap();
		map.setMyLocationEnabled(true);
		map.getUiSettings().setMyLocationButtonEnabled(false);
		MapsInitializer.initialize(this.getActivity());

		Log.i(TAG,latitude+" "+longitude);

		// Updates the location and zoom of the MapView
		CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude), 6);
//		map.addMarker(new MarkerOptions().position(new LatLng(latitude,longitude)).title("Your current location"));
		map.animateCamera(cameraUpdate);
		return v;
	}

	@Override
	public void onResume() {
		URL url;
		try {
			url = new URL(
					"http://infectious-agent.appspot.com/db?type=getlocations&email="+
							App.getStringFromPrefs(PreferencesHelper.LAST_USER_EMAIL));
			new GetLocationsTask().execute(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onResume();
		mapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapView.onLowMemory();
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		latitude = location.getLatitude();
		longitude = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public class GetLocationsTask extends AsyncTask<URL, Void, JSONObject> {

		protected JSONObject doInBackground(URL... urls) {
			try {
				return App.networkManager.createJSONfromRequest(urls[0]);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			catch (NullPointerException e) {
				e.printStackTrace();
			}
			return null;
		}
		protected void onPostExecute(JSONObject obj){
			try {
				fillMap(obj);
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}

	}

	public void fillMap(JSONObject obj) {
		try {
			JSONArray locations = obj.getJSONArray("locations");
			for(int i=0;i<locations.length();i++){
				Log.i(TAG,locations.getString(i));
				String location = locations.getString(i);
				double json_latitude = Double.parseDouble(location.split(";")[0]);
				double json_longitude = Double.parseDouble(location.split(";")[1]);
				map.addMarker(new MarkerOptions().position(new LatLng(json_latitude,json_longitude)));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}


}