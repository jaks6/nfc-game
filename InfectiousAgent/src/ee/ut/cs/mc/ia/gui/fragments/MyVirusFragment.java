package ee.ut.cs.mc.ia.gui.fragments;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import ee.ut.cs.mc.ia.App;
import ee.ut.cs.mc.ia.PreferencesHelper;
import ee.ut.cs.mc.ia.R;
import ee.ut.cs.mc.ia.Utilities;
import ee.ut.cs.mc.ia.Virus;

public class MyVirusFragment extends Fragment {

	private String TAG = "VIRUS_FRAGMENT";

	TextView tvVirusName;
	TextView tvVirusDescription;
	TextView tvVirusVictims;
	Handler mHandler;
	SharedPreferences prefs;

	public static final int MSG_UPDATE_UI = 1;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.myvirusfragment,
				container, false);

		tvVirusName = (TextView) rootView.
				findViewById(R.id.text_virus_name);
		tvVirusDescription = (TextView) rootView.
				findViewById(R.id.text_virus_description);
		tvVirusVictims = (TextView) rootView.
				findViewById(R.id.text_no_of_victims);
		
		mHandler = new Handler() { 
		    @Override 
		    public void handleMessage(Message msg) { 
		      if (msg.what == MSG_UPDATE_UI ){
		    	  fillTextViews(App.getUserVirus());
		      }
		    } 
		  }; 

		Virus userVirus = App.getUserVirus();
		if ( userVirus != null){
			fillTextViews(userVirus);
		} else{
		
			showDialogBox();
	
		}
		
		return rootView;
	}
	
	

	private void fillTextViews(Virus virus){
		Log.d(TAG, "fillTextViews");
			tvVirusName.setText(virus.name);
			tvVirusDescription.setText(virus.description);
			tvVirusVictims.setText(String.valueOf(virus.victims));
	}


	private void showDialogBox() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());


		LayoutInflater li = LayoutInflater.from(getActivity());
		final View promptsView = li.inflate(R.layout.newviruslayout, null);
		// set title
		alertDialogBuilder.setTitle("Choose a virus");
		final EditText etName = (EditText) promptsView.findViewById(R.id.newvirus_name);
		final EditText etDesc = (EditText) promptsView.findViewById(R.id.newvirus_description);

		// set dialog message
		alertDialogBuilder
		.setMessage("You don't have a virus! Create one yourself or get a randomized one.")
		.setCancelable(false)
		.setView(promptsView)
		.setNeutralButton("RANDOMIZE", null)
		.setPositiveButton("OK",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {


				String virusName = etName.getText().toString();
				virusName = Uri.encode(virusName);
				String virusDescription = etDesc.getText().toString();
				virusDescription = Uri.encode(virusDescription);

				JSONObject json = new JSONObject();
				try {
					json.put("type","virusanduser");
					json.put("name",virusName);
					json.put("description",virusDescription);
					json.put("owner",App.getStringFromPrefs(PreferencesHelper.LAST_USER_EMAIL));
					json.put("ownername",App.getStringFromPrefs(PreferencesHelper.USER_NAME));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				App.getNetworkManager().storeFromJSON(json,"POST", mHandler);
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		final AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Button b = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
				b.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View view) {
						JSONObject json = App.utilities.getRandomVirus();
						try {
							etName.setText(json.getString("name"));
							etDesc.setText(json.getString("description"));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			}
		});

		// show it
		alertDialog.show();
	}


}
