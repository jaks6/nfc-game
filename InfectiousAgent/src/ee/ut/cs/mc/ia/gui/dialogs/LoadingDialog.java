package ee.ut.cs.mc.ia.gui.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Message;
import android.util.Log;

public class LoadingDialog extends ProgressDialog {

	public static boolean VIRUS_DATA_FETCHED = false;
	
	Context context;

	protected static String TAG = LoadingDialog.class.toString();
	public LoadingDialog(Context context, String title, String message){
		super(context);
		this.setTitle(title);
		this.setMessage(message);
		this.show();
	}
	
	
/**
 * @param progress - the progressdialog to cancel
 * @param flags - the flags which all must be true before the dialog is canceled
 */
	public static void cancelWhenFlagsUp(final ProgressDialog progress, boolean... flags){
//		new Thread(new Runnable() {
//			@Override
//			public void run()
//			{
//				while (true) {
////					Log.d(TAG , "flags=" + flags[0]);
////					for (boolean flag:flags){
////						if (!flag){
////							try {
////								Thread.sleep(1000);
////							} catch (InterruptedException e) {
////								e.printStackTrace();
////							}
////							break;
//						}
//						//if we reach this code then all flags were true.
//						
//						//Tell FirstFragment to dismiss the loading dialgo
//						Message msg = new Message();
//						msg.what = FirstFragment.MSG_DISMISS_LOADING;
//						msg.sendToTarget();
//						return;
//					}
//				}
//			}
//		}).start();

	}


}
