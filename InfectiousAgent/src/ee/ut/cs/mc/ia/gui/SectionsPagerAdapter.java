package ee.ut.cs.mc.ia.gui;

import java.util.Locale;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import ee.ut.cs.mc.ia.App;
import ee.ut.cs.mc.ia.activity.MainActivity.DummySectionFragment;
import ee.ut.cs.mc.ia.gui.fragments.MyVirusFragment;
import ee.ut.cs.mc.ia.gui.fragments.TopTenFragment;
import ee.ut.cs.mc.ia.gui.fragments.MapFragment;
import ee.ut.cs.mc.ia.R;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

	public SectionsPagerAdapter(FragmentManager fragmentManager) {
		super(fragmentManager);
	}

	@Override
	public Fragment getItem(int position) {

		switch (position){
		case 0:
			return new MyVirusFragment();
		case 1:
			return new TopTenFragment();
		case 2:
			return new MapFragment();
		}
		
		// getItem is called to instantiate the fragment for the given page.
		// Return a DummySectionFragment (defined as a static inner class
		// below) with the page number as its lone argument.
		Fragment fragment = new DummySectionFragment();
		Bundle args = new Bundle();
		args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return 3;
	}
	
	protected String getStringFromResources(int resourceCode){
		return App.getContext().getResources().getString(resourceCode);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		Locale l = Locale.getDefault();
		switch (position) {
		case 0:
			return getStringFromResources(R.string.title_section1).toUpperCase(l);
		case 1:
			return getStringFromResources(R.string.title_section2).toUpperCase(l);
		case 2:
			return getStringFromResources(R.string.title_section3).toUpperCase(l);
		}
		return null;
	}
}