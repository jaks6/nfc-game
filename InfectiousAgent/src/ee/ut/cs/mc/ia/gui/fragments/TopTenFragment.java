package ee.ut.cs.mc.ia.gui.fragments;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import ee.ut.cs.mc.ia.App;
import ee.ut.cs.mc.ia.R;

public class TopTenFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.toptenfragment,
		container, false);
	
	
		try {
			GetTopTenTask t = new GetTopTenTask(rootView);
			t.execute(new URL("http://infectious-agent.appspot.com/db?type=gettopten"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return rootView;
	}
	
	
	public class GetTopTenTask extends AsyncTask<URL, Void, JSONObject> {
		
		View rootView;
		
		public GetTopTenTask(View v) {
			super();
			this.rootView=v;
		}
		protected JSONObject doInBackground(URL... urls) {
			try {
				return App.networkManager.createJSONfromRequest(urls[0]);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			 catch (NullPointerException e) {
				e.printStackTrace();
			}
			return null;
		}
		protected void onPostExecute(JSONObject obj){
			try {
				displayTopTenAgents(rootView, obj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	public void displayTopTenAgents(View rootView, JSONObject json) throws JSONException, IOException{
		TextView t = (TextView) rootView.findViewById(R.id.top_ten_agents);
		TextView t2 = (TextView) rootView.findViewById(R.id.top_ten_owners);
		String top10 = "";
		String top10_2 ="";
		for(int i=0;i<json.getJSONArray("topten").length();i++){
			try {
				top10+= (i+1) +".	"+json.getJSONArray("topten").get(i)+"\n";
				top10_2+= json.getJSONArray("toptenowners").get(i)+"\n";
			} catch (JSONException e) {
				e.printStackTrace();
			}
			t.setText(top10);
			t2.setText(top10_2);
		}
	}
}
