package ee.ut.cs.mc.ia;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.util.Log;

public class NfcManager{

	Activity activity;
	public NfcAdapter mNfcAdapter;
	NdefMessage mNdefMessage;
	
	String TAG = "NFCManager";
	
	PendingIntent mNfcPendingIntent;
	IntentFilter[] mNdefExchangeFilters;

	public NfcManager(Activity activity) {
		this.activity = activity;
		mNfcAdapter = NfcAdapter.getDefaultAdapter(activity);
	}
	public NfcManager() {
		this.mNfcAdapter = NfcAdapter.getDefaultAdapter(App.getContext());
	}





	public void sendNdefMessage(Activity activity) {
		String virusId = App.getUserVirus().id;
		
		String query = String.format("?virus=%s",virusId);
		String requestURL = "http://infectious-agent.appspot.com/nfc"+query;
		byte[] virusObject = null;
		try {
			virusObject = App.utilities.serialize(App.getUserVirus());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Log.i(TAG,requestURL);
		
		NdefRecord uriRecord = new NdefRecord(
			    NdefRecord.TNF_ABSOLUTE_URI ,
			    requestURL.getBytes(Charset.forName("US-ASCII")),
			    new byte[0], new byte[0]);
		
		
		// create an NDEF message with two records of plain text type
		mNdefMessage = new NdefMessage(
				new NdefRecord[] {
						uriRecord,                                                                                                                                                                                                                                                                                                           
						NdefRecord.createMime("application/ee.ut.cs.mc.ia", virusObject)}

				);

		
		mNfcAdapter.setNdefPushMessage(mNdefMessage,activity);
	}

	public static NdefRecord createNewTextRecord(String text, Locale locale, boolean encodeInUtf8) {
		byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));

		Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
		byte[] textBytes = text.getBytes(utfEncoding);

		int utfBit = encodeInUtf8 ? 0 : (1 << 7);
		char status = (char)(utfBit + langBytes.length);

		byte[] data = new byte[1 + langBytes.length + textBytes.length];
		data[0] = (byte)status;
		System.arraycopy(langBytes, 0, data, 1, langBytes.length);
		System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

		return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
	}


}