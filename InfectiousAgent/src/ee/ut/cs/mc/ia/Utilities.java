package ee.ut.cs.mc.ia;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.TextView;




public class Utilities {
	String TAG = "Utilities";
	public static JSONObject locationJSON;
	
	
	public JSONObject loadJSONFromAsset(String name) throws JSONException {
        String json = null;
        try {
            InputStream is = App.getContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
		return new JSONObject(json);
    }
	
	public JSONObject getRandomVirus(){
		JSONObject json = null;
		try {
			JSONArray viruses = (JSONArray) loadJSONFromAsset("viruses.json").get("viruses");
			json = viruses.getJSONObject(randInt(0,9));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public static int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public boolean isLocationEnabled(){
		return App.locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}
	
	public boolean isNFCEnabled(){
		return App.getNfcManager().mNfcAdapter.isEnabled();
	}
	
	public boolean isInternetEnabled(){
		ConnectivityManager connectivityManager 
        = (ConnectivityManager) App.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		  return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public void checkSettingsEnabled(final Context context){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		final String action;
	
		if(!isNFCEnabled() && !isLocationEnabled()){
			builder.setMessage("Please enable NFC touch, Location services and Internet!");
			action=android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;
			setButton(builder, context, action);
			builder.show();
		}
		else if(!isNFCEnabled()){
			builder.setMessage("Please enable NFC touch!");
			action=android.provider.Settings.ACTION_NFC_SETTINGS;
			setButton(builder, context, action);
			builder.show();
		}
		else if(!isLocationEnabled()){
			builder.setMessage("Please enable Location services!");
			action=android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS;
			setButton(builder, context, action);
			builder.show();
		}
		else{
			
		}
		
	}
	
	public void setButton(AlertDialog.Builder builder, final Context context, final String action){
		builder.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                context.startActivity(new Intent(action));
            }
        });
	}
	

	public static byte[] serialize(Object obj) throws IOException {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ObjectOutputStream os = new ObjectOutputStream(out);
	    os.writeObject(obj);
	    return out.toByteArray();
	}
	public Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
	    ByteArrayInputStream in = new ByteArrayInputStream(data);
	    ObjectInputStream is = new ObjectInputStream(in);
	    return is.readObject();
	}
	
	public void createDialog(Activity activity,String message){
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		LayoutInflater inflater = activity.getLayoutInflater();

		builder.setView(inflater.inflate(R.layout.infected_dialog, null));

		builder.setMessage(message);
		builder.setNegativeButton("Ok",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
		AlertDialog dialog = builder.show();
		TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
		messageText.setGravity(Gravity.CENTER);
		dialog.show();
	}
	



}
