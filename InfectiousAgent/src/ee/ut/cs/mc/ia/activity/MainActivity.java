package ee.ut.cs.mc.ia.activity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ee.ut.cs.mc.ia.App;
import ee.ut.cs.mc.ia.PreferencesHelper;
import ee.ut.cs.mc.ia.R;
import ee.ut.cs.mc.ia.Virus;
import ee.ut.cs.mc.ia.gui.ViewPagerHelper;


public class MainActivity extends FragmentActivity {

	private static final String TAG = "MainActivity";

	ViewPagerHelper mViewPagerHelper;

	private Handler mHandler;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mViewPagerHelper = new ViewPagerHelper(this);
		
	
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop");
	}

	@Override
	public void onResume() {
		Log.d(TAG, "Resuming");
		super.onResume();
		App.utilities.checkSettingsEnabled(this);
		// Check to see that the Activity started due to an Android Beam
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
			try {
				processIntent(getIntent());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}



	/**
	 * Parses the NDEF Message from the intent and prints to the TextView
	 * @throws JSONException 
	 * @throws UnsupportedEncodingException 
	 */
	void processIntent(Intent intent) throws UnsupportedEncodingException, JSONException{
		Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
				NfcAdapter.EXTRA_NDEF_MESSAGES);
		// (presumably) only one message sent during the beam
		NdefMessage msg = (NdefMessage) rawMsgs[0];
		Virus virusFromNFC = null;
		try {
			 virusFromNFC = (Virus) App.utilities.deserialize(msg.getRecords()[1].getPayload());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String virusID = virusFromNFC.id;
		String useremail = App.getStringFromPrefs(PreferencesHelper.LAST_USER_EMAIL);
		
		Log.i(TAG,virusFromNFC.toString());
		JSONObject json = new JSONObject();
		json.put("type","infection");
		json.put("virus",virusID);
		json.put("victim",useremail);
		json.put("location", App.getLocation().getLatitude()+";"+App.getLocation().getLongitude());
		
		App.getNetworkManager().storeFromJSON(json,"POST", mHandler);
		App.utilities.createDialog(this, "You've got "+ virusFromNFC.name);
		

	}


	public void SendNFC(View view){
		App.getNfcManager().sendNdefMessage(this);
	}


	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);
			TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.section_label);
			dummyTextView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));
			return rootView;
		}


	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			return false;
		case R.id.action_login:
			startActivity(new Intent(this,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			return false;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	

}
