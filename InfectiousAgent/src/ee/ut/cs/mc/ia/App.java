package ee.ut.cs.mc.ia;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import ee.ut.cs.mc.ia.network.NetworkManager;

public class App extends Application {
	private static Context mContext;
	
	public static SharedPreferences mPrefs;
    public static PreferencesHelper preferencesHelper;
    public static NetworkManager networkManager;
    public static NfcManager nfcManager;
    public static LocationManager locationManager;
    public static LocationListener locationListener;
    public static Utilities utilities;
    
    
    static Virus userVirus;
    static Location location;
 // Acquire a reference to the system Location Manager
    
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this.getApplicationContext();
        
        
        //mode private. only this app can read these preferences
      	mPrefs = mContext.getSharedPreferences(PreferencesHelper.USER_STORAGE_FILE, 
      			Context.MODE_PRIVATE);
      	networkManager = new NetworkManager();
      	nfcManager = new NfcManager();
      	preferencesHelper = new PreferencesHelper(mPrefs);
      	
      	utilities = new Utilities();
      	locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        initLocationTracking();
      	
    }

	private void initLocationTracking() {
		// Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
              // Called when a new location is found by the network location provider.
              App.setLocation(location);
            }
         
			public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
          };
          locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
	}
    
    public static Location getLocation(){
    	return location;
    }
    
    public static Context getContext(){
        return mContext;
    }
    public static SharedPreferences getPreferences(){
    	return mPrefs;
    }
    public static String getStringFromPrefs(String key){
    	return mPrefs.getString(key, null);
    }
    public static NetworkManager getNetworkManager(){
    	return networkManager;
    }
    public static NfcManager getNfcManager(){
    	return nfcManager;
    }
    
    
    public static Virus getUserVirus(){
    	return userVirus;
    }

	public static void setUserVirus(Virus virus) {
		if (virus != null) userVirus = virus;
	}


	public static void setUserVirus(JSONObject obj) throws JSONException {
		if (obj != null) userVirus = new Virus(obj);
	}
	public static void setLocation(Location newLocation){
		location = newLocation;
	}
}
