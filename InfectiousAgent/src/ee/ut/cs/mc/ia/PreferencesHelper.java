package ee.ut.cs.mc.ia;

import android.content.SharedPreferences;

public class PreferencesHelper {

	public static String LAST_USER_EMAIL = "last_user_email";
	public static String USER_NAME = "user_name";
	public static String USER_STORAGE_FILE = "usr_info";
	public static String USER_VIRUS_ID = "usr_virus_id";
	public static String USER_VIRUS_NAME = "usr_virus_name";
	public static String USER_VIRUS_DESCRIPTION = "usr_virus_description";
	public static String USER_VICTIMS_COUNT = "usr_victioms_count";
	
	SharedPreferences preferences;
	private static String TAG = "PreferencesHelper";
	
	
	public PreferencesHelper(SharedPreferences preferences){
		this.preferences = preferences;
	}
	public String get(String key){
		return preferences.getString(key, null);
	}
	
	public void store(String key, String value) {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(key, value);
		editor.commit();
	}
}
