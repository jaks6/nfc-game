package ee.ut.cs.mc.ia;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class Virus implements Serializable{
	
	public String id;
	public String name;
	public String description;
	public int victims;
	private String TAG = Virus.class.toString();
	

	public Virus(JSONObject virusJSON) throws JSONException {
		if (virusJSON != null && !virusJSON.getString("id").equals("-1")){
			this.id = virusJSON.getString("id");
			this.name = virusJSON.getString("name");
			this.name=this.name.replace("%20", " ");
			this.name=this.name.replace("%2C", ",");
			this.name=this.name.replace("%2E", ".");
			
			this.description = virusJSON.getString("description");
			
			this.description=this.description.replace("%20", " ");
			this.description=this.description.replace("%2C", ",");
			this.description=this.description.replace("%2E", ".");
			
			if (virusJSON.has("victims")){
				this.victims = Integer.parseInt(virusJSON.getString("victims"));
			} else {
				//The virus was *just* created client-side
				this.victims = 0;
			}
			
		}
	}
	
	public Virus(String name, String description){
		this.name = name;
		this.description = description;
		this.victims = 0;
	}
	
	public String toString(){
		return id+" "+name+" "+description;
		
	}

}
