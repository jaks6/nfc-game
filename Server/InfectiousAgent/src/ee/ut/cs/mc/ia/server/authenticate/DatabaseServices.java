package ee.ut.cs.mc.ia.server.authenticate;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class DatabaseServices {

	public static String checkUserExists(String email){
		String result = "N//A";
		Entity entitity = null;

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Key k = KeyFactory.createKey("User", email);
		try {
			entitity = datastore.get(k);
		} catch (EntityNotFoundException e1) {
			result = "No user with such e-mail!"; 
		}

		if (entitity == null) { result = "No user with such e-mail!"; }
		else { result = (String) entitity.getProperty("Name")  + "   ;   " +  email; }

		return result;
	}



	/**
	 * @param log 
	 * @param args
	 */
	public static ArrayList<String> getUsers(Logger log) {

		ArrayList<String> resultArray = new ArrayList<String>();

		// Get the Datastore Service
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();


		// Use class Query to assemble a query
		Query q = new Query("User");
		// Use PreparedQuery interface to retrieve results
		PreparedQuery pq = datastore.prepare(q);

		for (Entity result : pq.asIterable()) {
			String firstName = (String) result.getProperty("Name");
			String gmail = result.getKey().toString();
			log.info("sdad");
			log.info(firstName);
			log.info(gmail);


			resultArray.add(firstName  + ", " + gmail );
		}

		return resultArray;

	}


	public static void storeUser(String email, String name){
		Key key = KeyFactory.createKey("User", email);
		Entity greeting = new Entity("User", key);
		greeting.setProperty("Name", name);

		DatastoreService datastore =
				DatastoreServiceFactory.getDatastoreService();
		datastore.put(greeting);

	}


	/** returns null if email is not owner of any virus */
	public static JSONObject getVirusJSON(String ownerEmail) {
		JSONObject obj = new JSONObject();
		Entity entitity = null;
		String Name = "Virus Name";
		String Description = "Virus Description";
		String Victims = "No of infected people";

		DatastoreService datastore =
				DatastoreServiceFactory.getDatastoreService();

		Filter ownerFilter =
				new FilterPredicate("user_email",
						FilterOperator.EQUAL,
						ownerEmail);

		// Use class Query to assemble a query
		Query q = new Query("Virus").setFilter(ownerFilter);

		// Use PreparedQuery interface to retrieve results
		PreparedQuery pq = datastore.prepare(q);
		try {
			entitity = pq.asSingleEntity();
			
			if (entitity == null) return null;
			Name = (String) entitity.getProperty("Name");
			Description = (String) entitity.getProperty("Description");
			Victims = String.valueOf((Long) entitity.getProperty("Victims"));
		} catch (TooManyResultsException e) {
			e.printStackTrace();
		}
		try {
			obj.put("victims", Victims);
			obj.put("description", Description);
			obj.put("name", Name);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;

	}

}
