package ee.ut.cs.mc.ia.server;

import java.util.List;
import java.util.logging.Logger;





import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class DatabaseServices {
	static Logger log = Logger.getLogger("DatabaseServices");
	static DatastoreService datastore =
			DatastoreServiceFactory.getDatastoreService();
	private static DatabaseServices instance = null;

	public static DatabaseServices getInstance() {
		if(instance == null) {
			instance = new DatabaseServices();
		}
		return instance;
	}


	public void storeUser(String email, String name){
		storeUser(email, name, false);
	}
	public void storeUser(String email, String name, boolean temporary){
		Entity entity = new Entity("User", email);
		entity.setProperty("Name", name);
		entity.setProperty("Temporary", temporary);
		datastore.put(entity);
	}

	public Entity getUser(String email){
		Key key = KeyFactory.createKey("User", email);
		try {
			return datastore.get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	public String getVirusName(Long virusID){
		Entity entitity = getVirus(virusID);
		if (entitity == null) return "";
		return (String) entitity.getProperty("Name");
	}
	public JSONObject getTopTenJSON(){
		// Order by victim count, more to less
		Query q = new Query("Virus");
		q.addSort("Victims", SortDirection.DESCENDING);
		PreparedQuery pq = datastore.prepare(q);
		List<Entity> list = pq.asList(FetchOptions.Builder.withLimit(10));
		JSONObject resultJSON = new JSONObject();
		JSONArray toptenarray = new JSONArray();
		JSONArray toptenarrayOwners = new JSONArray();
		for (Entity e  : list){
			toptenarray.put(e.getProperty("Name"));
			toptenarrayOwners.put(e.getProperty("user_email"));
		}
		try {
			resultJSON.put("topten", toptenarray);
			resultJSON.put("toptenowners", toptenarrayOwners);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resultJSON;
	}
	
	public JSONObject getInfectionLocationsOfUser(String userEmail){
		Filter infectionFilter =
				new FilterPredicate("Infecter",
						FilterOperator.EQUAL,
						userEmail);

		// Use class Query to assemble a query
		Query q = new Query("Infection").setFilter(infectionFilter);
		PreparedQuery pq = datastore.prepare(q);
		List<Entity> list = pq.asList(FetchOptions.Builder.withLimit(90)); //Hardcode
		JSONObject resultJSON = new JSONObject();
		JSONArray coordinatesArray = new JSONArray();
		for (Entity e  : list){
			coordinatesArray.put(e.getProperty("Location"));
		}
		try {
			resultJSON.put("locations", coordinatesArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resultJSON;
		
	}



	public void storeNewInfection(String victim, Long virusId, String location){
		storeNewInfection(victim, virusId, false, location);
	}
	public void storeNewInfection(String victim, Long virusId, boolean temporary, String location){
		log.info(String.format("attemtping to store new infection, victim =%s, virusiD= %s, temporary=%b", victim, virusId, temporary));
		//If an infection entity with given victim and virusID exists, then
		//we would be creating a duplicate, return without storing new entitiy
		if (checkIfAlreadyInfected(victim, virusId)){
			//!TODO
			//Notify clients that victim already is infected
			return;
		}
		else {
			//Store new infection
			Entity entity = new Entity("Infection");
			entity.setProperty("Victim", victim);
			entity.setProperty("Temporary", temporary);
			entity.setProperty("Virus", virusId);
			
			String owner = getOwnerOfVirus(virusId);
			entity.setProperty("Infecter", owner);
			
			entity.setProperty("Location", location);
			datastore.put(entity);
			
			//Update viruses victims count.
			Entity virus = getVirus(virusId);
			virus.setProperty("Victims",( Long)virus.getProperty("Victims")+1);
			datastore.put(virus);
		}
	}

	private Entity getVirus(Long virusId) {
		Entity entitity = null;
		Key key = KeyFactory.createKey("Virus",
				(virusId));
		try {
			entitity = datastore.get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return entitity;
	}


	/**
	 * 
	 * @param name
	 * @param description
	 * @param owner
	 * @return key of the stored virus, as a string
	 */
	public String storeNewVirus(String name, String description, String owner){
		Entity entity = new Entity("Virus");
		entity.setProperty("Name", name);
		entity.setProperty("Description", description);
		entity.setProperty("user_email", owner);
		entity.setProperty("Victims", 0);

		return Long.toString(datastore.put(entity).getId());
	}




	public String getOwnerOfVirus(Long virus) {
		Entity entitity = null;
		Key key = KeyFactory.createKey("Virus",
				(virus));
		try {
			entitity = datastore.get(key);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			return "";
		}
		return (String) entitity.getProperty("user_email");
	}

	public boolean checkUserAlreadyExists(String email){
		Entity entitity = null;
		Key key = KeyFactory.createKey("User",
				(email));
		try {
			entitity = datastore.get(key);
		} catch (EntityNotFoundException e) {
			return false;
		}
		return true;
	}
	private boolean checkIfAlreadyInfected(String victim, Long virusId) {
		Query q = new Query("Infection");
		Filter filter1 =
				new FilterPredicate("Virus",
						Query.FilterOperator.EQUAL,
						virusId);
		Filter filter2 =
				new FilterPredicate("Victim",
						Query.FilterOperator.EQUAL,
						victim);

		Filter comp_filter = CompositeFilterOperator.and(filter1,
				filter2);

		return checkIfExists(q, comp_filter);
	}


	private boolean checkIfExists(Query query, Filter filter){
		query.setFilter(filter);

		PreparedQuery pq = datastore.prepare(query);
		return ( pq.asSingleEntity()!=null);
	}


	public boolean upgradeUserFromTemporary(String email){
		boolean response = false;
		if (checkUserAlreadyExists(email)){
			Transaction txn = datastore.beginTransaction();
			try {
			    Entity user = getUser(email);
			    if(user != null) response = true;
			    user.setProperty("Temporary", false);
			    datastore.put(user);
			    txn.commit();
			} finally {
			    if (txn.isActive()) {
			        txn.rollback();
			    }
			}
			upgradeInfectionsOfUserFromTemporary(email);
		}
		return response;
	}
	private void upgradeInfectionsOfUserFromTemporary(String email) {
		Iterable<Entity> entitities = null;

		Filter filter =
				new FilterPredicate("Victim",
						Query.FilterOperator.EQUAL,
						email);

		Query q = new Query("Infection").setFilter(filter);
		PreparedQuery pq = datastore.prepare(q);
		entitities = pq.asIterable();
		
		if(entitities != null){
			for (Entity e : entitities){
				Transaction txn = datastore.beginTransaction();
				e.setProperty("Temporary", false);
				datastore.put(e);
			    txn.commit();
			}
		}
		
	}


	/** returns null if email is not owner of any virus */
	public JSONObject getVirusJSON(String ownerEmail) {
		Entity entitity = null;
		String Id = "-1";
		String Name = "Virus Name";
		String Description = "Virus Description";
		String Victims = "No of infected people";

		Filter ownerFilter =
				new FilterPredicate("user_email",
						FilterOperator.EQUAL,
						ownerEmail);

		// Use class Query to assemble a query
		Query q = new Query("Virus").setFilter(ownerFilter);

		// Use PreparedQuery interface to retrieve results
		PreparedQuery pq = datastore.prepare(q);
		try {
			entitity = pq.asSingleEntity();

			if (entitity == null) return null;
			Name = (String) entitity.getProperty("Name");
			Description = (String) entitity.getProperty("Description");
			Victims = String.valueOf((Long) entitity.getProperty("Victims"));
			Id = String.valueOf(entitity.getKey().getId());
		} catch (TooManyResultsException e) {
			e.printStackTrace();
		}
		return createVirusJSON(Id, Name, Description, Victims);
	}

	private JSONObject createVirusJSON(String Id, String Name,
			String Description, String Victims) {
		JSONObject obj = new JSONObject();
		try {
			obj.put("victims", Victims);
			obj.put("description", Description);
			obj.put("name", Name);
			obj.put("id", Id);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return obj;
	}



}
