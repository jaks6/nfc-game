package ee.ut.cs.mc.ia.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class NfcServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 455085767477870161L;

	/** if request parameter "type" equals "virus", the response will search for the virus
	 * which belongs to the email provided in the parameter "email". If no virus is tied
	 * to provided email, response will have a code 404
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		log("NfcServlet, doGet");
		log("getURI="+req.getRequestURI());
		log("getreqURL="+req.getRequestURL());
		log("getQueryString="+req.getQueryString());


		if (req.getParameter("virus") != null){
			resp.setContentType("text/html");
			PrintWriter out = resp.getWriter();
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Infected</title>");
			out.println("<link rel='stylesheet' type='text/css' href='" + req.getContextPath() +  "/styles.css' />");
			out.println("</head>");
			if (req.getParameter("victim") != null){
				//If victim is defined, this means that the request was sent by the app, to notify that the user has been infected
			} else {
				//If victim is null but virus isn't null, this means the request comes from an url, meaning that the victim didn't have
				//the app installed
				handleNoAppInfection(req, out);
			}
		}

	}
	private void handleNoAppInfection(HttpServletRequest req, PrintWriter out) {
		DatabaseServices dbServices = DatabaseServices.getInstance();

		UserService userService = UserServiceFactory.getUserService();
		String thisURL = req.getRequestURI();

		Long virusId = Long.parseLong(req.getParameter("virus"));
		String infectionLocation = req.getParameter("location");
		String virusName = dbServices.getVirusName(virusId);
		String virusOwner = dbServices.getOwnerOfVirus(virusId);
		out.println(getInfectedParagraph(virusName, virusOwner));
		if (req.getUserPrincipal() != null) {
			String userEmail = req.getUserPrincipal().getName();

			//check if such an user already exists
			if (! dbServices.checkUserAlreadyExists(userEmail)){
				out.println("<p>To start infecting others, download the app at the <br/> " +
						"<a class='playstore' href='https://play.google.com/store/apps/details?id=ee.ut.cs.mc.ia'>Play Store</a></p> "+ ""
//						"!  You can <a href=\"" +
//						userService.createLogoutURL(thisURL) +
//						"\">sign out</a>."
						);
				dbServices.storeUser(userEmail, userService.getCurrentUser().getNickname(), true);
				//add an infection
				dbServices.storeNewInfection(userEmail, virusId, true, infectionLocation);
			} else {
				//Handle the case where infected player has an user but doesn't have the app installed
				out.println("<p>It seems that you have don't have the app installed. Get it here: <a class='playstore' href='https://play.google.com/store/apps/details?id=ee.ut.cs.mc.ia'>Play Store</a></p>");
			}
		} else {
			out.println("<a class='button' href=\"" +
					userService.createLoginURL(thisURL+"?"+req.getQueryString()) +
					"\">CREATE ACCOUNT</a>");
		}
	}
	/** returns the portion before the @-symbol in an e-mail. For example,
	 * with an input "foo.bar@ut.ee" it will return a string "foo bar"
	 */
	private String parseName(String userEmail) {
		String name = ((userEmail.split("@"))[0]).replace('.', ' ');
		return name;
	}



	String getInfectedParagraph(String virusName, String virusOwner ){
		if (virusOwner!= null && virusName !=null){
			String output ="<h2>" + virusOwner + " has infected you with </br>" + virusName + ". </h2> ";
			output+= "<p>You too, can infect " +
					"all types of life forms, from animals and plants to bacteria and archaea by signing in with " +
					"a google account: </p>";
			return output;
		} else return "error, virusowner or virusname null";
	}

}
