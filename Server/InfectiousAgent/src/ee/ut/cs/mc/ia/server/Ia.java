package ee.ut.cs.mc.ia.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class Ia extends HttpServlet {
	/** if request parameter "type" equals "virus", the response will search for the virus
	 * which belongs to the email provided in the parameter "email". If no virus is tied
	 * to provided email, response will have a code 404
	 */
	String TYPE_GETVIRUSINFO = "getvirus";
	String TYPE_GETTOPTEN = "gettopten";
	String TYPE_GETLOCATIONS = "getlocations";
	String TYPE_STOREINFECTION = "infection";
	String TYPE_STOREVIRUSANDUSER = "virusanduser";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();
		DatabaseServices dbServices = DatabaseServices.getInstance();
		
		// CHECK FOR REQUEST TYPE AND HANDLE ACCORDINGLY:
		if (req.getParameter("type").equals(TYPE_GETVIRUSINFO)) {
			//!TODO Handle case where user signed up from web login, doesn't have virus
			String virusOwner = req.getParameter("email");
			resp.setContentType("application/json");
			JSONObject obj = dbServices.getVirusJSON(virusOwner);
			if (obj == null){
				//This might be a user coming on from the web signup. See if they have pending infections.
				if (!dbServices.upgradeUserFromTemporary(virusOwner));
				resp.setStatus( 404 );
			} else {
				out.print(obj);
				out.flush();
			}
		} else if (req.getParameter("type").equals(TYPE_GETTOPTEN)) {
			JSONObject obj = dbServices.getTopTenJSON();
			if (obj == null){
				resp.setStatus( 404 );
			} else {
				out.print(obj);
				out.flush();
			}
		} else if (req.getParameter("type").equals(TYPE_GETLOCATIONS)) {
			String virusOwner = req.getParameter("email");
			JSONObject obj = dbServices.getInfectionLocationsOfUser(virusOwner);
			if (obj == null){
				resp.setStatus( 404 );
			} else {
				out.print(obj);
				out.flush();
			}
		}
		else{
			out.println("<h1>" + "This is a placeholder "+ "</h1>");
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		DatabaseServices dbServices = DatabaseServices.getInstance();
		// first, set the "content type" header of the response
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();

		if(req.getParameter("type").equals(TYPE_STOREINFECTION)) {
			if (req.getParameter("virus") != null){
				Long virusId = Long.parseLong(req.getParameter("virus"));
				
				if (req.getParameter("victim") != null && 
						req.getParameter("location") != null ){
					String victimName = req.getParameter("victim");
					String infectionLocation = req.getParameter("location");
					//Store new infection
					dbServices.storeNewInfection(victimName, virusId, infectionLocation);
					out.println("<h1>" + "Stored "+ "</h1>");
				} else {
					//If victim is null but virus isn't null, this means the request comes from an url, meaning that the victim didn't have
					//the app installed
				}
			}
			
		} else if(req.getParameter("type").equals(TYPE_STOREVIRUSANDUSER)){
				String name = req.getParameter("name");
				String description = req.getParameter("description");
				String owner = req.getParameter("owner");
				String ownerName = req.getParameter("ownername");
				
				if (name!=null && description!=null && owner!=null && ownerName!= null){
					//Store new virus
					String virusID = dbServices.storeNewVirus(name,description,owner);
					out.print(virusID);
					
					//Store new user
					dbServices.storeUser(owner, ownerName);
				}
		}
		// Close the writer; the response is done.
		out.close();
	}



}
